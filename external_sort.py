import tempfile
import os
import uuid
import argparse
import sys


class RecordReader:
    """
    Reads records from given file line by line using given separator.
    Each record is returned as an array.
    """

    def __init__(self, file_name, separator=','):
        self.file_name = file_name
        self.separator = separator

    def __enter__(self):
        self.file = open(self.file_name)
        return self

    def __iter__(self):
        return self

    def __next__(self):
        line = self.file.readline()
        if not line:
            raise StopIteration
        return line.split(self.separator)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()


class RecordWriter:
    """
    Writes records to given file using defined separator
    """

    def __init__(self, file_name, separator=','):
        self.file_name = file_name
        self.separator = separator

    def __enter__(self):
        self.file = open(self.file_name, 'w')
        return self

    def write(self, item):
        self.file.write(self.separator.join(item))

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()


def temporary_file_name():
    """
    Returns temporary file name
    """
    return os.path.join(tempfile.gettempdir(), str(uuid.uuid1()))


def read_or_none(reader):
    """Helper function to read record and return it if possible"""
    try:
        return reader.__next__()
    except StopIteration:
        return None


class TemporaryFile:
    """
    Used to automatically remove temporary files
    """

    def __enter__(self):
        self.file_name = temporary_file_name()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if os.path.exists(self.file_name):
            os.remove(self.file_name)


class ExternalSorter:
    """
    Sort records from input_file_path and writes sorted sequence into output_file_path
    Records are sorted using given comparator.
    """
    def sort(self,
             input_file_path,
             output_file_path,
             comparator,
             separator):
        n = self._count_items(input_file_path, separator)
        self._sort_internal(n, input_file_path, output_file_path, comparator, separator)

    @staticmethod
    def _count_items(file_path, separator):
        count = 0
        with RecordReader(file_path, separator) as reader:
            for _ in reader:
                count = count + 1
        return count

    @staticmethod
    def _sort_internal(n,
                       input_file_path,
                       output_file_path,
                       comparator,
                       separator):
        if n < 2:
            with RecordReader(input_file_path, separator) as reader:
                with RecordWriter(output_file_path, separator) as writer:
                    for item in reader:
                        writer.write(item)
        else:
            # split into two files
            first_file_size = n // 2

            with TemporaryFile() as first_file:
                with TemporaryFile() as second_file:

                    with RecordReader(input_file_path, separator) as reader:
                        with RecordWriter(first_file.file_name, separator) as writer:
                            for _ in range(first_file_size):
                                writer.write(reader.__next__())
                        with RecordWriter(second_file.file_name, separator) as writer:
                            for _ in range(n - first_file_size):
                                writer.write(reader.__next__())

                    with TemporaryFile() as sorted_first_file:
                        with TemporaryFile() as sorted_second_file:
                            ExternalSorter._sort_internal(first_file_size,
                                                          first_file.file_name,
                                                          sorted_first_file.file_name,
                                                          comparator,
                                                          separator)

                            ExternalSorter._sort_internal(n - first_file_size,
                                                          second_file.file_name,
                                                          sorted_second_file.file_name,
                                                          comparator,
                                                          separator)

                            with RecordReader(sorted_first_file.file_name, separator) as sorted_first_reader:
                                with RecordReader(sorted_second_file.file_name, separator) as sorted_second_reader:
                                    with RecordWriter(output_file_path, separator) as writer:
                                        first_item = read_or_none(sorted_first_reader)
                                        second_item = read_or_none(sorted_second_reader)
                                        while (first_item is not None) or (second_item is not None):
                                            if (first_item is not None) and (second_item is not None):
                                                if comparator(first_item, second_item) <= 0:
                                                    writer.write(first_item)
                                                    first_item = read_or_none(sorted_first_reader)
                                                else:
                                                    writer.write(second_item)
                                                    second_item = read_or_none(sorted_second_reader)
                                            else:
                                                if first_item is not None:
                                                    writer.write(first_item)
                                                    first_item = read_or_none(sorted_first_reader)
                                                if second_item is not None:
                                                    writer.write(second_item)
                                                    second_item = read_or_none(sorted_second_reader)


class ItemComparator:
    """
    Used to compare records in ascending order starting from the given column
    """
    def __init__(self, column_index):
        self.column_index = column_index

    def __call__(self, x, y):
        column = min(self.column_index, len(x), len(y))
        for i in range(column, len(x)):
            if x[i] < y[i]:
                return -1
            if x[i] > y[i]:
                return 1
        return 0


def build_comparator(column_index, reverse):
    """
    Construct comparator
    """
    ascending_order_comparator = ItemComparator(column_index)
    return (lambda x, y: ascending_order_comparator(y, x)) if reverse else \
        (lambda x, y: ascending_order_comparator(x, y))


def get_argument_parser():
    """
    Setup parser for command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("path",
                        type=str,
                        help="path to a file for sorting")
    parser.add_argument("-o",
                        "--output",
                        help="output file (by default it will be input file with '.output' suffix", )
    parser.add_argument("-r",
                        "--reverse",
                        help="sort in descending order",
                        action="store_true")
    parser.add_argument("-s",
                        "--separator",
                        help="field separator",
                        default=",",
                        type=str)
    parser.add_argument("-i",
                        "--column-index",
                        type=int,
                        default=0,
                        help="sort by column with given index")
    return parser


def validate_arguments(args):
    """
    Additional validation for command line arguments
    """
    input_path = args.path
    if not os.path.exists(input_path):
        print("file '{}' does not exist".format(input_path))
        sys.exit(-1)

    if args.column_index < 0:
        print("column index must be greater or equal to zero")
        sys.exit(-1)

    output_path = args.output or (input_path + '.output')

    return {
        'input_path': input_path,
        'output_path': output_path,
        'reverse': args.reverse,
        'column_index': args.column_index,
        'separator': args.separator
    }


if __name__ == "__main__":
    parser = get_argument_parser()
    if len(sys.argv) == 1:
        parser.print_help()
    else:
        args = parser.parse_args()
        validated_arguments = validate_arguments(args)

        ExternalSorter().sort(input_file_path=validated_arguments['input_path'],
                              output_file_path=validated_arguments['output_path'],
                              comparator=build_comparator(
                                  validated_arguments['column_index'],
                                  validated_arguments['reverse']),
                              separator=validated_arguments['separator'])
